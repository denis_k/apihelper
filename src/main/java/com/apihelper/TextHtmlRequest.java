package com.apihelper;

import com.apihelper.parsers.TextParser;

import java.io.IOException;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.Response;

public class TextHtmlRequest extends Request<String> {
    private final TextParser parser;

    public TextHtmlRequest(String url) {
        this(url, null, new TextParser(), null, null);
    }

    public TextHtmlRequest(String url, Listener<String> listener, ErrorListener errorListener) {
        this(url, null, new TextParser(), listener, errorListener);
    }

    public TextHtmlRequest(String url, Map<String, String> headers, TextParser parser,
                           Listener<String> listener, ErrorListener errorListener) {
        this("GET", url, headers, null, parser, listener, errorListener);
    }

    public TextHtmlRequest(String method, String url, Map<String, String> headers, RequestBody body,
                           TextParser parser, Listener<String> listener, ErrorListener errorListener) {
        super(method, url, headers, body, listener, errorListener);
        this.parser = parser;
    }

    @Override
    public String parseNetworkResponse(Response response) throws IOException {
        byte[] bytes = response.body().bytes();
//        L.logLong("parseNetworkResponse", new String(bytes));
        return parser == null ? null : parser.parse(bytes);
    }

    @Override
    public Error parseNetworkError(Response response) throws IOException {
        if (parser == null) {
            return super.parseNetworkError(response);
        } else {
            Error error = parser.error(response.body().bytes());
            error.code = response.code();
            return error;
        }
    }
}