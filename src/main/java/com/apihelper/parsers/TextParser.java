package com.apihelper.parsers;

import com.apihelper.utils.L;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * Created by denis on 20.01.16.
 */
public class TextParser extends Parser<String> {

    @Override
    public String parse(byte[] data) {
        return new String(data);
    }
}
