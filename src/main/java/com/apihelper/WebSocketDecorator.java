package com.apihelper;


import android.os.Build;

import com.apihelper.parsers.JsonParser;
import com.apihelper.parsers.Parser;
import com.apihelper.utils.L;
import com.apihelper.utils.SslSocketFix;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public abstract class WebSocketDecorator extends WebSocketListener {
    public static final String TAG = WebSocketDecorator.class.getSimpleName();

    public static final String CLOSED_BY_USER_REASON = "closed_by_user_reason";
    public static final String TYPE_KEY = "TYPE_KEY";
    public static final String DATA_KEY = "DATA_KEY";
    public static final String USER_SEND_TYPE_KEY = "USER_SEND_TYPE_KEY";
    private OkHttpClient client;
    private okhttp3.Request.Builder requestBuilder;
    private Map<String, String> keys = new HashMap<>();
    private BehaviorMediator mediator;
    private volatile WebSocket instance;

    private volatile AtomicBoolean isClosed = new AtomicBoolean(true);
    private static final List<String> delayed = new Vector<>();

    {
        keys.put(TYPE_KEY, "destination");
        keys.put(DATA_KEY, "data");
        keys.put(USER_SEND_TYPE_KEY, "user.send_message");
    }

    private Parser<Message> messageParser = new TextParser();

    public boolean isClosed() {
        return isClosed.get();
    }

    public WebSocketDecorator(String endpoint, BehaviorMediator mediator) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);
        if (Build.VERSION.SDK_INT < 22) {
            SslSocketFix.fix(clientBuilder);
        }
        // Create request for remote resource.
        requestBuilder = new okhttp3.Request.Builder()
                .url(endpoint);
        this.mediator = mediator;
        client = clientBuilder.build();
    }

    public WebSocketDecorator addKey(String key, String value) {
        keys.put(key, value);
        return this;
    }

    private void sendDelayed() {
        if (!delayed.isEmpty()) {
            for (String data : delayed) {
                instance.send(data);
            }
            delayed.clear();
        }
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        synchronized (this) {
            L.log(TAG + ":onOpen", "call");
            instance = webSocket;
            isClosed.set(false);
            sendDelayed();
            onConnected();
        }
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        L.log(TAG + ":onMessage", text);
        try {
            onMessage(messageParser.parse(text));
        } catch (IOException e) {
            e.printStackTrace();
            onFailure(webSocket, e, null);
        }
        sendDelayed();
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        L.log(TAG + ":onFailure", t.getMessage(), t);
        if (instance != null) {
            instance.cancel();
            instance = null;
        }
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        L.log(TAG + "onClosing", reason);
        isClosed.set(true);
        onDisconnected(code, reason);
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        synchronized (this) {
            L.log(TAG + ":onClosed", reason);
            delayed.clear();
            isClosed.set(true);
            instance = null;
        }
    }

    public synchronized void connect() {
        L.log(TAG + ":", "connect");
        if (isClosed.get()) {
            try {

                requestBuilder.headers(Headers.of(mediator.getHeaders()));

                // Execute the request and retrieve the response.
                client.newWebSocket(requestBuilder.build(), this);

                // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
                client.dispatcher().executorService().shutdown();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void sendMessage(final JsonNode data) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = mapper.createObjectNode();
        node.put(keys.get(TYPE_KEY), keys.get(USER_SEND_TYPE_KEY));
        node.set(keys.get(DATA_KEY), data);
        send(node.toString());
    }

    public synchronized void sendData(final ObjectNode data) {
        send(data.toString());
    }

    private void send(final String text) {
        if (instance != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (instance != null) {
                        boolean sent = instance.send(text);
                        L.log(TAG+":send", text);
                        if (!sent) {
                            delayed.add(text);
                            onDisconnected(0, "unknown");
                        }
                    } else {
                        delayed.add(text);
                    }
                }
            }).start();
        } else {
            delayed.add(text);
        }
    }

    public synchronized void close() {
        L.log(TAG + ":close", "call");
        client.dispatcher().cancelAll();
        if (instance != null) {
            try {
                instance.close(1000, CLOSED_BY_USER_REASON);
            } finally {
                isClosed.set(true);
            }
        }
    }

    public void onConnected() {
    }

    public abstract void onMessage(Message message);

    public void onDisconnected(int code, String reason) {
    }

    private class TextParser extends JsonParser<Message> {

        @Override
        public Message parse(JsonNode jsonNode) {
            Message message = new Message();
            message.type = jsonNode.path(keys.get(TYPE_KEY)).asText();
            message.data = jsonNode.path(keys.get(DATA_KEY));
            return message;
        }
    }

    public class Message {
        private String type = "";
        private JsonNode data;

        @Deprecated
        public String getDestination() {
            return type;
        }

        public String getType() {
            return type;
        }

        public JsonNode getData() {
            return data;
        }
    }
}